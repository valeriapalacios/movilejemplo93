package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    private EditText txtNum1;
    private EditText txtNum2;
    private TextView txtResult;
    private Button btnSumar, btnRestar, btnMultiplicar, btnDividir;
    private Button btnLimpiar, btnCerrar;
    private Operaciones op = new Operaciones(0.0f,0.0f);


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setEvents();
    }
    public void initComponents()
    {
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResult = (TextView) findViewById(R.id.txtRes);

        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMultiplicar = (Button) findViewById(R.id.btnMult);
        btnDividir = (Button) findViewById(R.id.btnDivi);

        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
    }

    public void setEvents()
    {
        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnDividir.setOnClickListener(this);
        this.btnMultiplicar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        //Realizar Operaciones, limpiar y Cerrar
        switch (view.getId())
        {
            case R.id.btnSuma:
                txtResult.setText("SIMON");
                break;

        }

    }
}
